angular.module('SchoolSpaStudent').directive('dayLessons', () ->
  restrict: 'E'
  replace: false
  scope:
    day: '@'
    lessons: '=lessons'
  templateUrl: '/assets/spa/student/partials/timetable_day.html'
)
