/**
 * Created by adlab on 21.01.16.
 */
import Mongoose from 'mongoose'
import timeStampsPlugin from 'mongoose-timestamps'

export const RequestSchema = Mongoose.Schema({
    uri: {type: String, default: '/'},
    host: {type: String, default: ''},
    refer: {type: String},
    "user-agent": {type: String},
    "accept-language": {type: String},
    "accept-encoding": {type: String},
    "accept": {type: String},
    device: {type: Mongoose.Schema.Types.ObjectId, reg: 'Device'}
});

RequestSchema.plugin(timeStampsPlugin);

export const Request = Mongoose.model('Request', RequestSchema);