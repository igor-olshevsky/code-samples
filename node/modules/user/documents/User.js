/**
 * Created by adlab on 21.01.16.
 */
import Mongoose from 'mongoose'
import authPlugin from 'passport-local-mongoose'
import timeStampsPlugin from 'mongoose-timestamps'

export const UserSchema = Mongoose.Schema({
    email: {type: String}
});

UserSchema.plugin(authPlugin)
UserSchema.plugin(timeStampsPlugin)

export const User = Mongoose.model('User', UserSchema)