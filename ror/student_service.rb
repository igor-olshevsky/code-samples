class Services::Student::StudentService
  def initialize(student)
    @student = student
  end

  def get_calendar_events(start_time = Date.today.beginning_of_week(:monday), end_time = Date.today.end_of_week(:monday))
    get_lessons(start_time, end_time) + get_events(start_time, end_time)
  end

  def get_lessons_grouped
    result = Hash.new
    @student.lessons.where(
        :group_id => Group.select(:id).where(
            :id => Distribution.select(:group_id).where(:student_id => @student.id).where(:status => Distribution::STATUS_ACTIVE)
        )
    ).order(:startTime => :desc, :day_num => :asc).group_by {|lesson| lesson.day}.each do |day, lessons|
      result[day] = lessons
    end
    lessons_serializer result
  end

  private
  def get_events(start_time, end_time)
    events = Event.where(:date.gte => start_time)
    .where(:date.lte => end_time)
    .where(meta: Student::META_NAME)
    .where(meta_id: @student.id)
    .where(type: Event::EVENT_TYPE)
  end

  def get_lessons(start_time, end_time)
    events = Event.where(:date.gte => start_time)
    .where(:date.lte => end_time)
    .where(meta: Group::META_NAME)
    .in(meta_id: @student.distributions.map {|distribution| distribution.group_id if distribution.status == Distribution::STATUS_ACTIVE})
    .where(type: Event::LESSON_TYPE)
  end

  def lessons_serializer grouped_lessons
    result = Hash.new
    grouped_lessons.each do |day, lessons|
      result[day] = {
          :day => day.capitalize,
          :lessons => lessons.map {|lesson|
            {

                :id => lesson.id,
                :teacher => lesson.teacher.user.login,
                :group => {
                    :title => lesson.group.title,
                    :id => lesson.group_id
                },
                :language => {
                    :id => lesson.group.language_id,
                    :title => lesson.group.language.title
                },
                :start => lesson.startTime.in_time_zone,
                :end => lesson.endTime.in_time_zone,
                :day => lesson.day,
                :room => lesson.classRoom,
                :day_num => lesson.day_num
            }
          }
      }
    end
    result.to_json
  end
end
